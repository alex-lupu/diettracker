# README #

This is a personal-use project used for easily preparing for and monitoring the progress of a daily organised diet plan. The intended features for this app are as follws:

* Preview for each day diet day
* Notifications and alerts for each meal and snack
* Progress tracking with photos
* Retrospective of past period

### How do I get set up? ###

This project uses **firebase**. Dependencies are managed using **CocoaPods**. In order to run this project:

* Clone the repository
* In the terminal app, go to the root directory of the project and run 
		
		pod install

* after this, open the project by running
		
		open DietTracker.xcworkspace
	or by double clicking the file in finder.

### Contribution guidelines ###

* the Master branch will always contain a stable product.
* any feature will be implemented on a separate branch rooted in the *develop* branch.
* any merge must be preceded by a pull request that includes at least the repository owner as a reviewer.

### Who do I talk to? ###

Lupu Stefan Alex
alex\_lupu1995@yahoo.com

Have fun!