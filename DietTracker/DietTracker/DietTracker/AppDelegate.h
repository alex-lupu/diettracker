//
//  AppDelegate.h
//  DietTracker
//
//  Created by Lupu Alex on 19/09/2017.
//  Copyright © 2017 Lupu Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

