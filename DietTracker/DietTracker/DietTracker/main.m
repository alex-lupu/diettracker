//
//  main.m
//  DietTracker
//
//  Created by Lupu Alex on 19/09/2017.
//  Copyright © 2017 Lupu Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
